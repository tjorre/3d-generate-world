// application.cpp

#include <zinc/event.hpp>
#include <SpriteFont.h>
#include <SpriteBatch.h>
#include "application.hpp"

Application::Application()
{
	m_frustrum = new zinc::Frustum;

	m_window = nullptr;
	m_width = 0;
	m_height = 0;

	m_keyboard.keys[0] = false;
	m_keyboard.keys[1] = false;
	m_keyboard.keys[2] = false;
	m_keyboard.keys[3] = false;

	m_mouse.position = zinc::Vector2(0.0f, 0.0f);
	m_mouse.buttons[0] = false;
	m_mouse.buttons[1] = false;

	m_previous = m_mouse.position;
}

Application::~Application()
{
}

bool Application::create(void* window, int32 width, int32 height)
{
	m_window = window;
	m_width = width;
	m_height = height;

		//sets handlers
	m_render_context = zinc::RenderContext::create(window, width, height);
	m_event_handler = zinc::EventHandler::create();

	m_event_handler->register_handler(this, &Application::on_event);

	zinc::Matrix4 projection = zinc::Matrix4::perspective(zinc::Math::to_rad(45.0f), (float32)m_width / (float32)m_height, 0.5f, 100.0f);
	m_othro_projection = zinc::Matrix4::orthogonal((float32)m_width, (float32)m_height, 0.0f, 1.0f);
	m_camera = zinc::Camera::create(projection);

	m_shader = zinc::ColorShader::create("assets/first_shader.txt");
	m_othro_shader = zinc::OthroShader::create("assets/othro_shader.txt");

	using namespace zinc;
	using namespace DirectX;

	m_sampler = zinc::Sampler::create(
		zinc::kTextureFilterMode_Linear,
		zinc::kTextureAddressMode_Clamp,
		zinc::kTextureAddressMode_Clamp);

	m_depth_state = zinc::DepthState::create(true, false);
	m_othro_depth_state = zinc::DepthState::create(false, false);
	//m_blend_state = zinc::BlendState::create();

	m_world.identity();
	m_world = zinc::Matrix4::translation(0.0f, 0.0f, 5.0f);
	
		//sets material and light
	m_material.Ka = 1.0f;
	m_material.Kd = 0.8f;
	m_material.Ks = 0.8f;
	m_material.shininess = 64.0f;

	m_light.direction = Vector4(0.0f, -1.0f, 0.0f, 0.0f);
	m_light.ambient = Vector4(0.5f, 0.5f, 0.5f, 1.0f);
	m_light.diffuse = Vector4(0.8f, 0.8f, 0.8f, 1.0f);
	m_light.eye = Vector4(0.0f, 0.0f, 0.0f, 0.0f);

		//creates game objects
	//skapar en texture, heightmap f�r terrain
	//ej en texture utan hur terrain ser ut
	//h�mtar �ven en texture fr�n "minecraft.png" och s�tter den till terrain
	m_terrain = zinc::Terrain::create("assets/heightmap.png", "assets/minecraft.png");
	m_terrain->set_camera(m_camera.get());	//.get h�mtar den faktiska pekaren som finns inuti uniquepointer
	m_terrain->set_color_shader(m_shader.get());

	//skapar texturen samt shader f�r skybox
	m_skybox = zinc::Skybox::create("assets/skybox.png", "assets/skybox_shader.txt");
	m_skybox->set_camera(m_camera.get());

	//skapar kuben
	m_cube = zinc::Cube::create("assets/minecraft.png");
	m_cube->set_camera(m_camera.get());
	m_cube->set_color_shader(m_shader.get());

	//skapar gui panel
	m_hmap4 = zinc::GUI::create("assets/heightmap4.png");
	m_hmap4->set_projection(m_othro_projection);
	m_hmap4->set_othro_shader(m_othro_shader.get());

	m_hmap2 = zinc::GUI::create("assets/heightmap2.png");
	m_hmap2->set_projection(m_othro_projection);
	m_hmap2->set_othro_shader(m_othro_shader.get());

	m_panel = zinc::GUI::create("assets/crate.png");
	m_panel->set_projection(m_othro_projection);
	m_panel->set_othro_shader(m_othro_shader.get());

	//skapar fonts
	m_sprite_font = new DirectX::SpriteFont(m_render_context->get_device(), L"assets/Arial_14_Regular.spritefont");
	m_sprite_batch = new DirectX::SpriteBatch(m_render_context->get_context());

	return true;
}

void Application::destroy()
{
	m_render_context->destroy();
}

void Application::update()
{
	//s�tter frustrum
	m_viewproject = m_camera->get_view() * m_camera->get_projection();
	m_frustrum->construct(m_viewproject);

	// note(tommi): update camera here
	on_click();
	
	m_camera->update();

	m_light.eye = zinc::Vector4(m_camera->get_position(), 0.0f);
	
				/////////////////////////////////

	//S�tter vilken shader som ska jobba och ritar ut den shadern 
	// note(tommi): clear the previous frame
	m_render_context->clear();

	// note(tommi): draw stuff
	//ritar ut terr�ngen, skybox
	m_skybox->draw();
	m_terrain->draw();
	m_terrain->set_wireframe(false);

	m_shader->set_material(m_material);
	m_shader->set_light(m_light);

		//ritar ut F�rsta kuben
	m_cube->set_position(zinc::Vector3(1.2f, 0.0f, 3.5f));
	if (m_frustrum->is_inside(*m_cube->m_axisbox))
	{
		m_cube->draw();
	}
		//ritar ut andra kuben
	m_cube->set_position(zinc::Vector3(-1.2f, 0.0f, 3.5f));
	if (m_frustrum->is_inside(*m_cube->m_axisbox))
	{
		m_cube->draw();
	}

		//ritar ut 2D plan
	m_hmap4->set_position(zinc::Vector3(20.0f, 20.0f, 0.0f));
	m_hmap4->draw();
	
	m_hmap2->set_position(zinc::Vector3(200.0f, 20.0f, 0.0f));
	m_hmap2->draw();

	m_panel->set_position(zinc::Vector3(m_mouse.position.m_x, m_mouse.position.m_y, 0.0f));
	m_panel->draw();

		//ritar font
	m_sprite_batch->Begin();
	m_sprite_font->DrawString(m_sprite_batch, L"Choose your heightmap:", DirectX::XMFLOAT2(0.0f, 0.0f), DirectX::Colors::DarkRed);
	m_sprite_batch->End();

		//anv�nder blendstate
	//m_blend_state->use();
	
	// note(tommi): present the result to the user
	m_render_context->present();
}

void Application::on_event(zinc::Event* event)
{
	using namespace zinc;

	switch (event->get_type())
	{
		case kEventType_MouseMove:
		{
			m_mouse.position = event->get_mouse();
		} break;
		case kEventType_MouseDown:
		{
			m_mouse.buttons[event->get_button()] = true;
		} break;
		case kEventType_MouseUp:
		{
			m_mouse.buttons[event->get_button()] = false;
		} break;

		case kEventType_KeyDown:
		{
			m_keyboard.keys[event->get_keycode()] = true;
		} break;
		case kEventType_KeyUp:
		{
			m_keyboard.keys[event->get_keycode()] = false;
		} break;
	}
}

void Application::on_click()
{
	if (m_mouse.buttons[zinc::kMouseButton_Right])
	{
		const float32 sensitivity = 0.005f;
		zinc::Vector2 diff = m_mouse.position - m_previous;
		if (fabsf(diff.m_y) > 0.0f)
		{
			m_camera->rotate_x(diff.m_y * sensitivity);
		}
		if (fabsf(diff.m_x) > 0.0f)
		{
			m_camera->rotate_y(diff.m_x * sensitivity);
		}
	}

	if (m_hmap4->m_axisbox->box_contains(zinc::Vector3(m_mouse.position.m_x, m_mouse.position.m_y, 0.0f)))
	{
		if (m_mouse.buttons[zinc::kMouseButton_Left])
		{
			m_terrain = zinc::Terrain::create("assets/heightmap4.png", "assets/minecraft.png");
			m_terrain->set_camera(m_camera.get());
			m_terrain->set_color_shader(m_shader.get());
		}
	}
	if (m_hmap2->m_axisbox->box_contains(zinc::Vector3(m_mouse.position.m_x, m_mouse.position.m_y, 0.0f)))
	{
		if (m_mouse.buttons[zinc::kMouseButton_Left])
		{
			m_terrain = zinc::Terrain::create("assets/heightmap2.png", "assets/minecraft.png");
			m_terrain->set_camera(m_camera.get());
			m_terrain->set_color_shader(m_shader.get());
		}
	}

	m_previous = m_mouse.position;

	const float32 movement_speed = 0.1f;
	if (m_keyboard.keys[zinc::kKeyCode_Up])
	{
		m_camera->move_z(movement_speed);
	}
	if (m_keyboard.keys[zinc::kKeyCode_Down])
	{
		m_camera->move_z(-movement_speed);
	}
	if (m_keyboard.keys[zinc::kKeyCode_Left])
	{
		m_camera->move_x(-movement_speed);
	}
	if (m_keyboard.keys[zinc::kKeyCode_Right])
	{
		m_camera->move_x(movement_speed);
	}
}