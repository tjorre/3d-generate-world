// winmain.cpp

#define Unused(x) ((void)x)

#include <Windows.h>
#include <Windowsx.h>

#include <zinc/service_locator.hpp>
#include <zinc/event_handler.hpp>
#include <zinc/event.hpp>
#include <application.hpp>

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	zinc::EventHandler* handler = zinc::ServiceLocator<zinc::EventHandler>::get_service();
	if (handler == nullptr) return DefWindowProc(hWnd, uMsg, wParam, lParam);
	
	switch (uMsg)
	{
		case WM_MOUSEMOVE:
		{
			zinc::Event event;
			event.m_type = zinc::kEventType_MouseMove;
			event.m_mouse.m_x = (float32)GET_X_LPARAM(lParam);
			event.m_mouse.m_y = (float32)GET_Y_LPARAM(lParam);
			handler->notify(&event);
		} break;

		case WM_LBUTTONDOWN:
		{
			zinc::Event event;
			event.m_type = zinc::kEventType_MouseDown;
			event.m_button = zinc::kMouseButton_Left;
			event.m_mouse.m_x = (float32)GET_X_LPARAM(lParam);
			event.m_mouse.m_y = (float32)GET_Y_LPARAM(lParam);
			handler->notify(&event);
		} break;
		case WM_LBUTTONUP:
		{
			zinc::Event event;
			event.m_type = zinc::kEventType_MouseUp;
			event.m_button = zinc::kMouseButton_Left;
			event.m_mouse.m_x = (float32)GET_X_LPARAM(lParam);
			event.m_mouse.m_y = (float32)GET_Y_LPARAM(lParam);
			handler->notify(&event);
		} break;

		case WM_RBUTTONDOWN:
		{
			zinc::Event event;
			event.m_type = zinc::kEventType_MouseDown;
			event.m_button = zinc::kMouseButton_Right;
			event.m_mouse.m_x = (float32)GET_X_LPARAM(lParam);
			event.m_mouse.m_y = (float32)GET_Y_LPARAM(lParam);
			handler->notify(&event);
		} break;
		case WM_RBUTTONUP:
		{
			zinc::Event event;
			event.m_type = zinc::kEventType_MouseUp;
			event.m_button = zinc::kMouseButton_Right;
			event.m_mouse.m_x = (float32)GET_X_LPARAM(lParam);
			event.m_mouse.m_y = (float32)GET_Y_LPARAM(lParam);
			handler->notify(&event);
		} break;

		case WM_KEYDOWN:
		case WM_KEYUP:
		{
			zinc::Event event;
			event.m_type = uMsg == 
				WM_KEYDOWN ? zinc::kEventType_KeyDown : zinc::kEventType_KeyUp;
			switch (wParam)
			{
				case VK_LEFT:
				{
					event.m_keycode = zinc::kKeyCode_Left;
				} break;
				case VK_RIGHT:
				{
					event.m_keycode = zinc::kKeyCode_Right;
				} break;
				case VK_UP:
				{
					event.m_keycode = zinc::kKeyCode_Up;
				} break;
				case VK_DOWN:
				{
					event.m_keycode = zinc::kKeyCode_Down;
				} break;
			}
			if (event.m_keycode != zinc::kKeyCode_Invalid)
			{
				handler->notify(&event);
			}
		} break;
		
		case WM_CLOSE:
		{
			PostQuitMessage(0);
		} break;
		default:
			return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}
	return 0;
}

int CALLBACK WinMain(HINSTANCE hInstance, 
	HINSTANCE hPrevInstance, 
	LPSTR lpCmdLine, 
	int nCmdShow)
{
	Unused(hInstance);
	Unused(hPrevInstance);
	Unused(lpCmdLine);
	Unused(nCmdShow);

	WNDCLASSEX window_class = { 0 };
	window_class.cbSize = sizeof(WNDCLASSEX);
	window_class.hInstance = hInstance;
	window_class.lpszClassName = L"zincwindowclass";
	window_class.lpfnWndProc = WndProc;
	window_class.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	window_class.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	int result = RegisterClassEx(&window_class);
	if (result == 0)
		return 1;

	DWORD style = (WS_OVERLAPPEDWINDOW & ~(WS_THICKFRAME | WS_MAXIMIZEBOX | WS_MINIMIZEBOX));

	int width = 1280;
	int height = 720;

	RECT rc = { 0, 0, width, height };
	AdjustWindowRect(&rc, style, FALSE);

	RECT deskrect = { 0 };
	HWND desktop = GetDesktopWindow();
	GetWindowRect(desktop, &deskrect);
	int centerx = deskrect.right / 2 - ((rc.right - rc.left) / 2);
	int centery = deskrect.bottom / 2 - ((rc.bottom - rc.top) / 2);

	HWND window = CreateWindowEx(
		0,
		window_class.lpszClassName,
		L"zinc",
		style,
		centerx, 
		centery,
		rc.right - rc.left, 
		rc.bottom - rc.top,
		NULL,
		NULL,
		window_class.hInstance,
		NULL);

	if (window == NULL)
		return 2;

	ShowWindow(window, nCmdShow);

	Application app;
	if (!app.create(window, width, height))
		return 3;

	bool running = true;
	while (running)
	{
		MSG msg;
		while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				running = false;
			}
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		app.update();
		Sleep(5);
	}

	app.destroy();

	return 0;
}

