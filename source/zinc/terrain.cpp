// terrain.cpp

#include <stdio.h>
#include <cassert>
#include <d3d11.h>

#include "stb_image.h"

#include <zinc/service_locator.hpp>
#include <zinc/render_context.hpp>
#include <zinc/color_shader.hpp>
#include <zinc/texture.hpp>
#include <zinc/sampler.hpp>
#include <zinc/camera.hpp>
#include <zinc/terrain.hpp>

#define SAFE_RELEASE(x) if(x != nullptr) { x->Release(); x = nullptr; }

namespace zinc
{
	// note(tommi): static 
	Terrain::Ptr Terrain::create(const char* filename, const char* texture_filename)
	{
		FILE* fin = fopen(filename, "rb");
		assert(fin && "could not open terrain file");
		fseek(fin, 0, SEEK_END);
		uint32 filesize = ftell(fin) + 1;
		fseek(fin, 0, SEEK_SET);
		uint8* filebuf = new uint8[filesize];
		fread(filebuf, 1, filesize, fin);
		fclose(fin);

		int32 width = 0, height = 0, components = 0;
		stbi_uc* bitmap = stbi_load_from_memory(filebuf, filesize, &width, &height, &components, 4);

		// todo(tommi): create terrain from heightmap
		uint32 count = width * height;
		Vertex* vertices = new Vertex[count];

		float32 scale_x = 0.5f;
		float32 scale_y = 0.05f;
		float32 scale_z = 0.5f;
		for (uint32 z = 0, i = 0; z < (uint32)height; z++)
		{
			float32 pz = z * scale_z;
      float32 ty = (float32)z;
			for (uint32 x = 0; x < (uint32)width; x++)
			{
				uint32 index = z * 4 * width + x * 4;
				float32 px = x * scale_x;
				float32 py = bitmap[index] * scale_y;
        float32 tx = (float32)x;

				vertices[i].position = Vector3(px, py, pz);
				vertices[i].texcoord = Vector2(tx, ty);
				vertices[i].normal = Vector3(0.0f, 0.0f, 0.0f);
				i++;
			}
		}

		uint32 num_indices = (width - 1) * (height - 1) * 6;
		uint16* indices = new uint16[num_indices]; 
		for (uint32 y = 0, i = 0; y < (uint32)height - 1; y++)
		{
			for (uint32 x = 0; x < (uint32)width - 1; x++)
			{
				indices[i++] = (uint16)(y * width + x + 1);
				indices[i++] = (uint16)(y * width + x);
				indices[i++] = (uint16)((y + 1) * width + x + 1);
				indices[i++] = (uint16)((y + 1) * width + x);
				indices[i++] = (uint16)((y + 1) * width + x + 1);
				indices[i++] = (uint16)(y * width + x);
			}
    }

    // note(tommi): calculate normals
    for (uint32 i = 0; i < num_indices; i += 3)
    {
      uint32 idx0 = indices[i + 0];
      uint32 idx1 = indices[i + 1];
      uint32 idx2 = indices[i + 2];

      Vector3& p0 = vertices[idx0].position;
      Vector3& p1 = vertices[idx1].position;
      Vector3& p2 = vertices[idx2].position;

      Vector3 q = p1 - p0;
      Vector3 r = p2 - p0;
      Vector3 n = q.cross(r);
      n.normalize();
      
      vertices[idx0].normal += n;
      vertices[idx1].normal += n;
      vertices[idx2].normal += n;
    }

    for (uint32 i = 0; i < count; i++)
    {
      vertices[i].normal.normalize();
    }

		Mesh::Ptr mesh = Mesh::create(sizeof(Vertex), count, vertices, 
			num_indices, indices);

		Terrain* result = new Terrain;
		result->m_count = num_indices;
		result->m_mesh = std::move(mesh);
		result->m_rasterizer_state[kWireframeState_On] = 
			RasterizerState::create(kCullMode_Back, kFillMode_WireFrame);
		result->m_rasterizer_state[kWireframeState_Off] =
			RasterizerState::create(kCullMode_Back, kFillMode_Solid);

		delete[] vertices;
		delete[] indices;

		stbi_image_free(bitmap);
		delete[] filebuf;
		
		return Ptr(result);
	}

	// note(tommi): private
	Terrain::Terrain()
	{
		m_shader = nullptr;
    m_texture = nullptr;
    m_sampler = nullptr;
		m_camera = nullptr;
		m_count = 0;
		m_state = kWireframeState_On;
	}

	// note(tommi): public
	Terrain::~Terrain()
	{
	}

	void Terrain::draw()
	{
		Matrix4 world;
		world.identity();

		m_shader->set_view_and_projection(
			m_camera->get_view(), 
			m_camera->get_projection());
		m_shader->set_world(world);
		m_shader->use();
    m_sampler->use();
    m_texture->use();
		m_rasterizer_state[m_state]->use();
		m_mesh->draw(0, m_count);
	}

	void Terrain::set_color_shader(ColorShader* shader)
	{
		m_shader = shader;
	}

  //void Terrain::set_texture(Texture& texture)
  //{
  //  m_texture = texture;
  //}

  //void Terrain::set_sampler(Sampler* sampler)
  //{
	 // m_sampler = sampler;
  //}

	void Terrain::set_camera(Camera* camera)
	{
		m_camera = camera;
	}

	void Terrain::set_wireframe(bool state)
	{
		m_state = state ? kWireframeState_On : kWireframeState_Off;
	}
}
