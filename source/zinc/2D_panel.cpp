#include <stdio.h>
#include <cassert>

#include <zinc\camera.hpp>
#include <zinc\2D_panel.hpp>

namespace zinc
{
	GUI::Ptr GUI::create(const char* texture_filename)
	{
		struct Othro_Vertex		//2D plan
		{
			zinc::Vector3 position;
			zinc::Vector2 texcoord;
		};

		Vector2 texcoords[] =
		{
			Vector2(0.0f, 0.0f),
			Vector2(1.0f, 0.0f),
			Vector2(1.0f, 1.0f),
			Vector2(0.0f, 1.0f),
		};

		//2D planet
		float h = 0.0f;
		float w = 100.0f;
		Vector3 positions2D[] =
		{
			Vector3(h,  h, 0.5f),
			Vector3(w,  h, 0.5f),
			Vector3(w, w, 0.5f),
			Vector3(h, w, 0.5f),
		};

		Othro_Vertex othro_vertices[] =
		{
			{ positions2D[0], texcoords[0] },
			{ positions2D[1], texcoords[1] },
			{ positions2D[2], texcoords[2] },
			{ positions2D[2], texcoords[2] },
			{ positions2D[3], texcoords[3] },
			{ positions2D[0], texcoords[0] },
		};

		uint32 count = sizeof(othro_vertices) / sizeof(othro_vertices[0]);
		Mesh::Ptr mesh = Mesh::create(
			sizeof(Othro_Vertex),
			count,
			othro_vertices,
			0, nullptr);

		GUI* result = new GUI;
		result->m_count = count;
		result->m_texture = Texture::create(texture_filename);
		result->m_mesh = std::move(mesh);
		result->m_depth_state = DepthState::create(false, false);
		result->m_axisbox = new AxisAlignedBoundingBox(Vector3(0.0f, 0.0f, 0.0f), Vector3(w, w, 0.5f));
		return Ptr(result);
	}

	GUI::GUI()
	{
		m_shader = nullptr;
		m_count = 0;
	}

	GUI::~GUI()
	{
	}

	void GUI::draw()
	{
		// note(tommi): example world transforms for two cubes
		Matrix4 world;
		world = Matrix4::translation(m_position);
		
		m_shader->set_projection(m_projection);
		m_shader->set_world(world);
		
		m_shader->use();
		m_texture->use();
		m_depth_state->use();

		m_mesh->draw(0, m_count);
	}

	void GUI::set_projection(Matrix4 projection)
	{
		m_projection = projection;
	}

	void GUI::set_othro_shader(OthroShader* shader)
	{
		m_shader = shader;
	}

	void GUI::set_position(Vector3 position)
	{
		m_position = position;
		m_axisbox->m_center = m_position;
	}

	Vector3 GUI::get_posistion()
	{
		return m_position;
	}
	
	void GUI::set_size(Vector3 size)
	{
		m_size = size;
	}
}