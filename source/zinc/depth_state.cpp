
#include <stdio.h>
#include <cassert>
#include <d3d11.h>

#include <zinc/service_locator.hpp>
#include <zinc/render_context.hpp>
#include <zinc/depth_state.hpp>

#define SAFE_RELEASE(x) if(x!=nullptr) { x->Release(); x = nullptr; }

namespace zinc
{
	DepthState::Ptr DepthState::create(bool read, bool write)
	{
		RenderContext* render_context = ServiceLocator<RenderContext>::get_service();
		ID3D11Device* device = render_context->get_device();

		D3D11_DEPTH_STENCIL_DESC desc = { 0 };
		desc.DepthEnable = read ? TRUE : FALSE;
		desc.DepthWriteMask = write ? D3D11_DEPTH_WRITE_MASK_ALL : D3D11_DEPTH_WRITE_MASK_ZERO;
		desc.DepthFunc = D3D11_COMPARISON_LESS;

		desc.StencilEnable = FALSE;
		desc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
		desc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
		
		desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
		desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		
		desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
		desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;

		ID3D11DepthStencilState* depth_state = nullptr;
		HRESULT hr = device->CreateDepthStencilState(&desc, &depth_state);
		if (hr != S_OK)
		{
			assert(false && "could not create depth stencil state");
		}

		DepthState* result = new DepthState;
		result->m_depth_state = depth_state;
		result->m_context = render_context->get_context();
		return Ptr(result);
	}

	DepthState::DepthState()
	{
		m_depth_state = nullptr;
		m_context = nullptr;
	}

	DepthState::~DepthState()
	{
		SAFE_RELEASE(m_depth_state);
	}

	void DepthState::use()
	{
		m_context->OMSetDepthStencilState(m_depth_state, 0);
	}
}