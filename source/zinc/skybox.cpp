
#include <stdio.h>
#include <cassert>

#include <zinc\camera.hpp>
#include <zinc\skybox.hpp>

namespace zinc
{
	Skybox::Ptr Skybox::create(const char* texture_filename, const char* shader_filename)
	{
		struct Vertex
		{
			Vector3 posistion;
			Vector2 texcoord;
		};

		//Denna utr�kningen �r f�r fron culling
		//world posistions
		float32 v = 5.0f;
		Vector3 positions[]=
		{
			Vector3(-v,  v, -v),
			Vector3(v,  v, -v),
			Vector3(v, -v, -v),
			Vector3(-v, -v, -v),

			Vector3(-v, v, v),
			Vector3(v, v, v),
			Vector3(v, -v, v),
			Vector3(-v, -v, v),
		};

		//texture cords
		const float32 o = 1.0f / 4.0f;
		const float32 p = 1.0f / 3.0f;
		Vector2 texcoords[]=
		{
			Vector2(o, 0), //textcoord 0 //top
			Vector2(2 * o, 0.0f),//textcoord 1
			Vector2(o, p),//textcoord 2
			Vector2(2 * o, p),//textcoord 3

			Vector2(0, p),//textcoord 4 //left
			Vector2(o, p),//textcoord 5
			Vector2(0, p * 2),//textcoord 6
			Vector2(o, p * 2),//textcoord 7

			Vector2(o, p),//textcoord 8 //front
			Vector2(o * 2, p),//textcoord 9
			Vector2(o, p * 2),//textcoord 10
			Vector2(o * 2, p * 2),//textcoord 11

			Vector2(o * 2, p),//textcoord 12 //right
			Vector2(o * 3, p),//textcoord 13
			Vector2(o * 2, p * 2),//textcoord 14
			Vector2(o * 3, p * 2),//textcoord 15

			Vector2(o * 3, p),//textcoord 16 //back
			Vector2(o * 4, p),//textcoord 17
			Vector2(o * 3, p * 2),//textcoord 18
			Vector2(o * 4, p * 2),//textcoord 19

			Vector2(o, p * 2),//textcoord 20 //bot
			Vector2(2 * o, p * 2),//textcoord 21
			Vector2(o, p * 3),//textcoord 22
			Vector2(2 * o, p * 3),//textcoord 23
		};

		//Skybox triangles
		Vertex vertices[] =
		{
			{ positions[0], texcoords[17] }, //uppe v�nster
			{ positions[1], texcoords[16] }, //uppe h�ger         back
			{ positions[2], texcoords[18] }, //nere h�ger
			{ positions[2], texcoords[18] }, //nere h�ger
			{ positions[3], texcoords[19] }, //nere v�nster
			{ positions[0], texcoords[17] }, // uppe v�nster

			{ positions[1], texcoords[13] }, //uppe h�ger
			{ positions[5], texcoords[12] }, //uppe v�nster
			{ positions[6], texcoords[14] },//nere v�nster
			{ positions[6], texcoords[14] },//nere v�nster
			{ positions[2], texcoords[15] }, // nere h�ger
			{ positions[1], texcoords[13] }, //uppe h�ger

			{ positions[4], texcoords[8] }, //uppe v�nster
			{ positions[7], texcoords[10] }, //nere v�nster
			{ positions[5], texcoords[9] }, //uppe h�ger				front
			{ positions[7], texcoords[10] }, // nere v�nser
			{ positions[6], texcoords[11] }, //nere h�ger
			{ positions[5], texcoords[9] }, //uppe h�ger

			{ positions[3], texcoords[6] }, //							left
			{ positions[4], texcoords[5] },
			{ positions[0], texcoords[4] },
			{ positions[3], texcoords[6] },
			{ positions[7], texcoords[7] },
			{ positions[4], texcoords[5] },

			{ positions[0], texcoords[0] }, //							top
			{ positions[4], texcoords[2] },
			{ positions[5], texcoords[3] },
			{ positions[5], texcoords[3] },
			{ positions[1], texcoords[1] },
			{ positions[0], texcoords[0] },

			{ positions[3], texcoords[22] }, //nere v�nster
			{ positions[2], texcoords[23] }, //nere h�ger
			{ positions[6], texcoords[21] }, //uppe h�ger
			{ positions[6], texcoords[21] }, //uppe h�ger
			{ positions[7], texcoords[20] }, //uppe v�nster
			{ positions[3], texcoords[22] },  //nere v�nster
		}; 

		uint32 count = sizeof(vertices) / sizeof(vertices[0]);
		Mesh::Ptr mesh = Mesh::create(
			sizeof(Vertex),
			count,
			vertices,
			0, nullptr);
		
		Skybox* result = new Skybox;
		result->m_count = count;
		result->m_shader = SkyboxShader::create(shader_filename);
		result->m_texture = Texture::create(texture_filename);
		result->m_mesh = std::move(mesh);
		result->m_sampler = Sampler::create(kTextureFilterMode_Linear, kTextureAddressMode_Clamp, kTextureAddressMode_Clamp);
		result->m_depth_state = DepthState::create(false, false);
		result->m_rasterizer_state = RasterizerState::create(kCullMode_Front, kFillMode_Solid);
		return Ptr(result);
	}

	Skybox::Skybox()
	{
		m_camera = nullptr;
		m_count = 0;
	}

	Skybox::~Skybox()
	{

	}

	void Skybox::draw()
	{
		if (!m_camera)
			return;

		//vi nollst�ller translationen f�r skyboxen,
		Matrix4 view = m_camera->get_view();
		view.m._41 = 0.0f;
		view.m._42 = 0.0f;
		view.m._43 = 0.0f;

		m_shader->set_view_and_projection(
			view,
			m_camera->get_projection());
		m_shader->use();
		m_sampler->use();
		m_texture->use();
		m_depth_state->use();
		m_rasterizer_state->use();
		m_mesh->draw(0, m_count);
	}

	void Skybox::set_camera(Camera* camera)
	{
		m_camera = camera;
	}
}