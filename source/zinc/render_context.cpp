// render_context.cpp

#include <cassert>
#include <dxgi.h>
#include <d3d11.h>
#pragma comment(lib, "d3d11.lib")
#pragma comment (lib, "dxguid.lib")

#define SAFE_RELEASE(x) if(x!=nullptr) { x->Release(); x = nullptr; }

#include "zinc/service_locator.hpp"
#include "zinc/render_context.hpp"

namespace zinc
{
	// note(tommi): static 
	RenderContext::Ptr RenderContext::create(void* window, int32 width, int32 height)
	{
		(void)window;
		(void)width;
		(void)height;

		IDXGISwapChain* swap_chain = nullptr;
		
		DWORD flags = D3D11_CREATE_DEVICE_SINGLETHREADED;
#if !NDEBUG
		//flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

		D3D_FEATURE_LEVEL feature_level_request = D3D_FEATURE_LEVEL_11_0;
		D3D_FEATURE_LEVEL feature_level;

		DXGI_SWAP_CHAIN_DESC scd = { 0 };
		scd.BufferDesc.Width = width;
		scd.BufferDesc.Height = height;
		scd.BufferDesc.RefreshRate.Denominator = 1;
		scd.BufferDesc.RefreshRate.Numerator = 60;
		scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		scd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		scd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
		scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		scd.BufferCount = 1;
		scd.SampleDesc.Count = 1;
		scd.SampleDesc.Quality = 0;
		scd.OutputWindow = (HWND)window;
		scd.Windowed = TRUE;
		scd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
		scd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

		ID3D11Device* device = nullptr;
		ID3D11DeviceContext* context = nullptr;

		HRESULT hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			D3D_DRIVER_TYPE_HARDWARE,
			NULL,
			flags,
			&feature_level_request,
			1,
			D3D11_SDK_VERSION,
			&scd,
			&swap_chain,
			&device,
			&feature_level,
			&context);
		if (hr != S_OK)
		{
			assert(false && "could not create swap-chain, device or context");
		}

		ID3D11Resource* resource = nullptr;
		hr = swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&resource);
		if (hr != S_OK)
		{
			assert(false && "could not get backbuffer");
		}

		ID3D11RenderTargetView* rtv = nullptr;
		hr = device->CreateRenderTargetView(resource, NULL, &rtv);
		if (hr != S_OK)
		{
			assert(false && "could not create render target view");
		}
		resource->Release();

		// note(tommi): depth stencil view
		D3D11_TEXTURE2D_DESC desc;
		desc.Width = width;
		desc.Height = height;
		desc.MipLevels = 1;
		desc.ArraySize = 1;
		desc.Format = DXGI_FORMAT_D32_FLOAT;
		desc.SampleDesc.Count = 1;
		desc.SampleDesc.Quality = 0;
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		desc.CPUAccessFlags = 0;
		desc.MiscFlags = 0;

		ID3D11Texture2D* depth_stencil = nullptr;
		hr = device->CreateTexture2D(&desc, NULL, &depth_stencil);
		if (hr != S_OK)
		{
			assert(false && "could not create depth stencil buffer");
		}

		ID3D11DepthStencilView* dsv = nullptr;
		hr = device->CreateDepthStencilView(depth_stencil, NULL, &dsv);
		if (hr != S_OK)
		{
			assert(false && "could not create depth stencil view");
		}

		context->OMSetRenderTargets(1, &rtv, dsv);

		D3D11_VIEWPORT viewport = { 0 };
		viewport.Width = (FLOAT)width;
		viewport.Height = (FLOAT)height;
		viewport.MinDepth = 0.0f;
		viewport.MaxDepth = 1.0f;
		viewport.TopLeftX = 0;
		viewport.TopLeftY = 0;
		context->RSSetViewports(1, &viewport);

		RenderContext* result = new RenderContext;
		result->m_swap_chain = swap_chain;
		result->m_device = device;
		result->m_context = context;
		result->m_rtv = rtv;
		result->m_depth_stencil_buffer = depth_stencil;
		result->m_dsv = dsv;
		return Ptr(result);
	}

	// note(tommi): private
	RenderContext::RenderContext()
	{
		ServiceLocator<RenderContext>::set_service(this);
	}

	// note(tommi): public
	RenderContext::~RenderContext()
	{
		ServiceLocator<RenderContext>::set_service(nullptr);
	}

	void RenderContext::destroy()
	{
		SAFE_RELEASE(m_dsv);
		SAFE_RELEASE(m_depth_stencil_buffer);
		SAFE_RELEASE(m_rtv);
		SAFE_RELEASE(m_context);
		SAFE_RELEASE(m_device);
		SAFE_RELEASE(m_swap_chain);
	}

	void RenderContext::clear()
	{
		// note(tommi): red, green, blue, alpha
		static const FLOAT color[4] = { 0.1f, 0.2f, 0.3f, 1.0f };
		m_context->ClearRenderTargetView(m_rtv, color);
		m_context->ClearDepthStencilView(m_dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);
	}

	void RenderContext::present()
	{
		m_swap_chain->Present(0, 0);
	}

	ID3D11Device* RenderContext::get_device()
	{
		return m_device;
	}

	ID3D11DeviceContext* RenderContext::get_context()
	{
		return m_context;
	}
}
