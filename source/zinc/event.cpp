// event.cpp

#include <zinc/event.hpp>

namespace zinc
{
	Event::Event()
	{
		m_type = kEventType_Invalid;
		m_button = kMouseButton_Invalid;
		m_keycode = kKeyCode_Invalid;
	}

	Event::Event(const Event& rhs)
	{
		m_type = rhs.m_type;
		m_mouse = rhs.m_mouse;
		m_button = rhs.m_button;
	}

	Event& Event::operator=(const Event& rhs)
	{
		m_type = rhs.m_type;
		m_mouse = rhs.m_mouse;
		m_button = rhs.m_button;
		return *this;
	}

	EventType Event::get_type() const
	{
		return m_type;
	}

	Vector2 Event::get_mouse() const
	{
		return m_mouse;
	}

	MouseButton Event::get_button() const
	{
		return m_button;
	}

	KeyCode Event::get_keycode() const
	{
		return m_keycode;
	}
}
