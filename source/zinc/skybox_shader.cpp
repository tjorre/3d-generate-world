
#include <stdio.h>
#include <cassert>
#include <d3d11.h>

#include <zinc/service_locator.hpp>
#include <zinc/render_context.hpp>
#include <zinc/skybox_shader.hpp>

namespace zinc
{
	SkyboxShader::Ptr SkyboxShader::create(const char* filename)
	{
		FILE* fin = fopen(filename, "r");
		assert(fin && "could not open skybox shader source file");
		fseek(fin, 0, SEEK_END);
		uint32 size = (uint32)ftell(fin);
		fseek(fin, 0, SEEK_SET);

		//char* source = new char[size + 1];
		char source[4096] = { 0 };
		assert(size < sizeof(source) && "skybox shader source too big for source buffer");
		uint32 len = (uint32)fread(source, sizeof(char), size, fin);
		fclose(fin);

		source[len] = '\0';

		char* vertex_source = strstr(source, "[vertex]");
		assert(vertex_source);
		*vertex_source = '\0';
		vertex_source += 8;

		char* pixel_source = strstr(vertex_source, "[pixel]");
		assert(pixel_source);
		*pixel_source = '\0';
		pixel_source += 7;

		return Ptr(new SkyboxShader(vertex_source, pixel_source));
	}

	SkyboxShader::SkyboxShader(const char* vertex_source, const char* pixel_source) : Shader(vertex_source, pixel_source)
	{
		RenderContext* render_context = ServiceLocator<RenderContext>::get_service();
		ID3D11Device* device = render_context->get_device();

		D3D11_INPUT_ELEMENT_DESC desc[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};

		HRESULT hr = device->CreateInputLayout(
			desc,
			sizeof(desc) / sizeof(desc[0]),
			m_layout_signature->GetBufferPointer(),
			m_layout_signature->GetBufferSize(),
			&m_input_layout);
		if (hr != S_OK)
		{
			assert(false && "could not create input layout");
		}

		D3D11_BUFFER_DESC buf_desc = { 0 };
		buf_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		buf_desc.ByteWidth = sizeof(Matrix4);
		buf_desc.CPUAccessFlags = 0;
		buf_desc.MiscFlags = 0;
		buf_desc.StructureByteStride = 0;
		buf_desc.Usage = D3D11_USAGE_DEFAULT;

		Matrix4 identity;
		identity.identity();

		D3D11_SUBRESOURCE_DATA data = { 0 };
		data.pSysMem = &identity;
		data.SysMemPitch = 0;
		data.SysMemSlicePitch = 0;

		hr = device->CreateBuffer(&buf_desc, &data, &m_once_per_frame);
		if (hr != S_OK)
		{
			assert(false && "could not create once per frame constant buffer");
		}

		hr = device->CreateBuffer(&buf_desc, &data, &m_once_per_object);
		if (hr != S_OK)
		{
			assert(false && "could not create once per object constant buffer");
		}
	}

	SkyboxShader::~SkyboxShader()
	{

	}

	void SkyboxShader::use()
	{
		Shader::use();

		ID3D11Buffer* bufs[] = { m_once_per_frame, m_once_per_object };
		m_context->VSSetConstantBuffers(0, 2, bufs);
	}

	void SkyboxShader::set_view_and_projection(const Matrix4& view, const Matrix4& projection)
	{
		Matrix4 view_proj = view * projection;
		m_context->UpdateSubresource(m_once_per_frame, 0, NULL, &view_proj, 0, 0);
	}
}