// sampler.cpp

#include <stdio.h>
#include <cassert>
#include <d3d11.h>

#include <zinc/service_locator.hpp>
#include <zinc/render_context.hpp>
#include <zinc/sampler.hpp>

#define SAFE_RELEASE(x) if(x!=nullptr) { x->Release(); x = nullptr; }

namespace zinc
{
	// note(tommi): static 
	Sampler::Ptr Sampler::create(TextureFilterMode filter, TextureAddressMode addr_u, TextureAddressMode addr_v)
	{
		RenderContext* render_context = ServiceLocator<RenderContext>::get_service();
		ID3D11Device* device = render_context->get_device();

		const D3D11_FILTER filters[] =
		{
			D3D11_FILTER_MIN_MAG_MIP_POINT,
			D3D11_FILTER_MIN_MAG_MIP_LINEAR,
		};

		const D3D11_TEXTURE_ADDRESS_MODE addresses[] =
		{
			D3D11_TEXTURE_ADDRESS_CLAMP,
			D3D11_TEXTURE_ADDRESS_WRAP,
			D3D11_TEXTURE_ADDRESS_MIRROR,
		};

		D3D11_SAMPLER_DESC desc;
		desc.AddressU = addresses[(uint32)addr_u];
		desc.AddressV = addresses[(uint32)addr_v];
		desc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		desc.BorderColor[0] = 0.0f;
		desc.BorderColor[1] = 0.0f;
		desc.BorderColor[2] = 0.0f;
		desc.BorderColor[3] = 0.0f;
		desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		desc.Filter = filters[(uint32_t)filter];
		desc.MaxAnisotropy = 1;
		desc.MaxLOD = D3D11_FLOAT32_MAX;
		desc.MinLOD = 0.0f;
		desc.MipLODBias = 0.0f;

		ID3D11SamplerState* sampler = nullptr;
		HRESULT hr = device->CreateSamplerState(&desc, &sampler);
		if (hr != S_OK)
		{
			assert(false && "could not create sampler");
		}

		Sampler* result = new Sampler;
		result->m_sampler_state = sampler;
		result->m_context = render_context->get_context();
		return Ptr(result);
	}

	// note(tommi): private 
	Sampler::Sampler()
	{
	}

	// note(tommi): public
	Sampler::~Sampler()
	{
		SAFE_RELEASE(m_sampler_state);
	}

	void Sampler::use() 
	{
		m_context->PSSetSamplers(0, 1, &m_sampler_state);
	}
}
