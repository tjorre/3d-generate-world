
#include <stdio.h>
#include <cassert>
#include <d3d11.h>

#include <zinc/service_locator.hpp>
#include <zinc/render_context.hpp>
#include <zinc/blend_state.hpp>

#define SAFE_RELEASE(x) if(x!=nullptr) { x->Release(); x = nullptr; }

namespace zinc
{
	BlendState::Ptr BlendState::create()
	{
		RenderContext* render_context = ServiceLocator<RenderContext>::get_service();
		ID3D11Device* device = render_context->get_device();

		D3D11_BLEND_DESC desc = { 0 };
		desc.AlphaToCoverageEnable = FALSE;
		desc.IndependentBlendEnable = FALSE;
		desc.RenderTarget[0].BlendEnable = FALSE;
		desc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
		desc.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
		desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

		ID3D11BlendState* blend_state = nullptr;
		HRESULT hr = device->CreateBlendState(&desc, &blend_state);
		if (hr != S_OK)
		{
			assert(false && "could not create blend state");
		}

		BlendState* result = new BlendState;
		result->m_blend_state = blend_state;
		result->m_context = render_context->get_context();
		return Ptr(result);
	}

	BlendState::BlendState()
	{
		m_blendFactor[0] = { 1};
		m_blendFactor[1] = { 1 };
		m_blendFactor[2] = { 1 };
		m_blendFactor[3] = { 1 };

		m_blend_state = nullptr;
		m_context = nullptr;
	}

	BlendState::~BlendState()
	{
		SAFE_RELEASE(m_blend_state);
	}

	void BlendState::use()
	{
		m_context->OMSetBlendState(m_blend_state, m_blendFactor, 0xffffffff);
	}
}