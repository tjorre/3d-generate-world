// shader.cpp

#include <stdio.h>
#include <cassert>

#include <d3d11.h>
#include <d3dcompiler.h>
#pragma comment(lib, "d3dcompiler.lib")

#include <zinc/service_locator.hpp>
#include <zinc/render_context.hpp>
#include <zinc/shader.hpp>

#define SAFE_RELEASE(x) if(x!=nullptr) { x->Release(); x = nullptr; }

namespace zinc
{
	// note(tommi): static 
	Shader::Ptr Shader::create(const char* filename)
	{
		FILE* fin = fopen(filename, "r");
		assert(fin && "could not open shader source file");
		fseek(fin, 0, SEEK_END);
		uint32 size = (uint32)ftell(fin);
		fseek(fin, 0, SEEK_SET);

		//char* source = new char[size + 1];
		char source[4096] = { 0 };
		assert(size < sizeof(source) && "shader source too big for source buffer");
		uint32 len = (uint32)fread(source, sizeof(char), size, fin);
		fclose(fin);

		source[len] = '\0';

		char* vertex_source = strstr(source, "[vertex]");
		assert(vertex_source);
		*vertex_source = '\0';
		vertex_source += 8;

		char* pixel_source = strstr(vertex_source, "[pixel]");
		assert(pixel_source);
		*pixel_source = '\0';
		pixel_source += 7;

		return Ptr(new Shader(vertex_source, pixel_source));
	}

	// note(tommi): protected
	Shader::Shader(const char* vertex_source, const char* pixel_source)
	{
		RenderContext* render_context = ServiceLocator<RenderContext>::get_service();
		ID3D11Device* device = render_context->get_device();
		m_context = render_context->get_context();

		ID3DBlob* source = nullptr, *error = nullptr;

		HRESULT hr = S_OK;
		hr = D3DCompile(
			vertex_source,
			strlen(vertex_source),
			NULL,
			NULL,
			NULL,
			"main",
			"vs_5_0",
			0, // flags
			0,
			&source,
			&error);
		if (hr != S_OK)
		{
			const char* error_message = (const char*)error->GetBufferPointer();
			OutputDebugStringA(error_message);
			assert(false && "error in vertex shader source");
		}

		hr = device->CreateVertexShader(
			source->GetBufferPointer(),
			source->GetBufferSize(),
			NULL,
			&m_vertex_shader);
		if (hr != S_OK)
		{
			assert(false && "could not create vertex shader");
		}

		D3DGetInputAndOutputSignatureBlob(
			source->GetBufferPointer(),
			source->GetBufferSize(),
			&m_layout_signature);

		SAFE_RELEASE(source);
		SAFE_RELEASE(error);

		hr = D3DCompile(
			pixel_source,
			strlen(pixel_source),
			NULL,
			NULL,
			NULL,
			"main",
			"ps_5_0",
			0, // flags
			0,
			&source,
			&error);
		if (hr != S_OK)
		{
			const char* error_message = (const char*)error->GetBufferPointer();
			OutputDebugStringA(error_message);
			assert(false && "could not compile pixel shader");
		}

		hr = device->CreatePixelShader(
			source->GetBufferPointer(),
			source->GetBufferSize(),
			NULL,
			&m_pixel_shader);
		if (hr != S_OK)
		{
			assert(false && "could not create pixel shader");
		}
		SAFE_RELEASE(source);
		SAFE_RELEASE(error);

		m_input_layout = nullptr;
	}

	// note(tommi): public
	Shader::~Shader()
	{
		SAFE_RELEASE(m_layout_signature);
		SAFE_RELEASE(m_input_layout);
		SAFE_RELEASE(m_vertex_shader);
		SAFE_RELEASE(m_pixel_shader);
	}

	void Shader::use()
	{
		m_context->VSSetShader(m_vertex_shader, NULL, 0);
		m_context->PSSetShader(m_pixel_shader, NULL, 0);
		m_context->IASetInputLayout(m_input_layout);
	}
}
