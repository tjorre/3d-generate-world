// event_handler.cpp

#include <zinc/service_locator.hpp>
#include <zinc/event_handler.hpp>

namespace zinc
{
	// note(tommi): static 
	EventHandler::Ptr EventHandler::create()
	{
		return Ptr(new EventHandler);
	}

	// note(tommi): private
	EventHandler::EventHandler()
	{
		ServiceLocator<EventHandler>::set_service(this);
	}

	// note(tommi): public
	EventHandler::~EventHandler()
	{
		ServiceLocator<EventHandler>::set_service(nullptr);

		auto itr = m_handlers.begin();
		while (itr != m_handlers.end())
		{
			delete (*itr);
			++itr;
		}
		m_handlers.clear();
	}

	void EventHandler::notify(Event* event)
	{
		auto itr = m_handlers.begin();
		while (itr != m_handlers.end())
		{
			(*itr)->invoke(event);
			++itr;
		}
	}
}
