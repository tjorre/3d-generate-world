// texture.cpp

#include <stdio.h>
#include <cassert>
#include <d3d11.h>

#define STB_IMAGE_IMPLEMENTATION 
#include "stb_image.h"

#include <zinc/service_locator.hpp>
#include <zinc/render_context.hpp>
#include <zinc/texture.hpp>

#define SAFE_RELEASE(x) if(x!=nullptr) { x->Release(); x = nullptr; }

namespace zinc
{
	// note(tommi): static 
	Texture::Ptr Texture::create(const char* filename)
	{
		RenderContext* render_context = ServiceLocator<RenderContext>::get_service();
		ID3D11Device* device = render_context->get_device();
		
		FILE* fin = fopen(filename, "rb");
		assert(fin);

		int width = 0, height = 0, components = 0;
		stbi_uc* bitmap = stbi_load_from_file(fin, &width, &height, &components, 4);
		fclose(fin);

		D3D11_TEXTURE2D_DESC desc = { 0 };
		desc.ArraySize = 1;
		desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		desc.CPUAccessFlags = 0;
		desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		desc.Width = width;
		desc.Height = height;
		desc.MipLevels = 1;
		desc.MiscFlags = 0;
		desc.SampleDesc.Count = 1;
		desc.SampleDesc.Quality = 0;
		desc.Usage = D3D11_USAGE_DEFAULT;

		D3D11_SUBRESOURCE_DATA data = { 0 };
		data.pSysMem = bitmap;
		data.SysMemPitch = width * components;
		data.SysMemSlicePitch = 0;

		ID3D11Texture2D* texture = nullptr;
		HRESULT hr = device->CreateTexture2D(&desc, &data, &texture);
		if (hr != S_OK)
		{
			assert(false && "could not create texture");
		}

		D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
		srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srv_desc.Format = desc.Format;
		srv_desc.Texture2D.MipLevels = desc.MipLevels;
		srv_desc.Texture2D.MostDetailedMip = 0;

		ID3D11ShaderResourceView* srv = nullptr;
		hr = device->CreateShaderResourceView(texture, &srv_desc, &srv);
		if (hr != S_OK)
		{
			assert(false && "could not create shader resource view for texture");
		}

		stbi_image_free(bitmap);

		Texture* result = new Texture;
		result->m_texture = texture;
		result->m_srv = srv;
		result->m_context = render_context->get_context();
		return Ptr(result);
	}

	// note(tommi): private
	Texture::Texture()
	{
	}

	// note(tommi) public
	Texture::~Texture()
	{
		SAFE_RELEASE(m_srv);
		SAFE_RELEASE(m_texture);
	}

	void Texture::use()
	{
		m_context->PSSetShaderResources(0, 1, &m_srv);
	}
}
