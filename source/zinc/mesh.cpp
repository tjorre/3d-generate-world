// mesh.cpp

#include <cassert>
#include <d3d11.h>

#include <zinc/service_locator.hpp>
#include <zinc/render_context.hpp>
#include <zinc/mesh.hpp>

#define SAFE_RELEASE(x) if(x!=nullptr) { x->Release(); x = nullptr; }

namespace zinc
{
	// note(tommi): static 
	Mesh::Ptr Mesh::create(uint32 size, uint32 count, const void* buffer, uint32 num_indices, const uint16* indices)
	{
		return Ptr(new Mesh(size, count, buffer, num_indices, indices));
	}

	// note(tommi) private
	Mesh::Mesh(uint32 size, uint32 count, const void* buffer, uint32 num_indices, const uint16* indices)
	{
		m_stride = size;

		RenderContext* render_context = ServiceLocator<RenderContext>::get_service();
		ID3D11Device* device = render_context->get_device();
		m_context = render_context->get_context();

		D3D11_BUFFER_DESC buf_desc = { 0 };
		buf_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		buf_desc.ByteWidth = size * count;
		buf_desc.CPUAccessFlags = 0;
		buf_desc.MiscFlags = 0;
		buf_desc.StructureByteStride = 0;
		buf_desc.Usage = D3D11_USAGE_DEFAULT;

		D3D11_SUBRESOURCE_DATA data = { 0 };
		data.pSysMem = buffer;
		data.SysMemPitch = 0;
		data.SysMemSlicePitch = 0;

		HRESULT hr = device->CreateBuffer(&buf_desc, &data, &m_vertex_buffer);
		if (hr != S_OK)
		{
			assert(false && "could not create vertex buffer");
		}

		m_index_buffer = nullptr;
		if (indices)
		{
			buf_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
			buf_desc.ByteWidth = sizeof(uint16) * num_indices;

			data.pSysMem = indices;

			hr = device->CreateBuffer(&buf_desc, &data, &m_index_buffer);
			if (hr != S_OK)
			{
				assert(false && "could not create index buffer");
			}
		}
	}

	// note(tommi): public
	Mesh::~Mesh()
	{
		SAFE_RELEASE(m_vertex_buffer);
		SAFE_RELEASE(m_index_buffer);
	}

	void Mesh::draw(uint32 start, uint32 count)
	{
		const uint32 offset = 0;
		m_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		m_context->IASetVertexBuffers(0, 1, &m_vertex_buffer, &m_stride, &offset);
		if (!m_index_buffer)
		{
			m_context->Draw(count, start);
		}
		else
		{
			m_context->IASetIndexBuffer(m_index_buffer, DXGI_FORMAT_R16_UINT, start);
			m_context->DrawIndexed(count, 0, 0);
		}
		//m_context->Draw(count, start);
	}
}
