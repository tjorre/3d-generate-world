// camera.cpp

#include <zinc/camera.hpp>

namespace zinc
{
	// note(tommi): static 
	Camera::Ptr Camera::create(const Matrix4& projection)
	{
		return Ptr(new Camera(projection));
	}

	// note(tommi): private
	Camera::Camera(const Matrix4& projection)
		: m_projection(projection)
	{
		m_dirty = true;
		m_view.identity();

		m_position = Vector3(0.0f, 0.0f, 0.0f);
		m_right = Vector3(1.0f, 0.0f, 0.0f);
		m_up = Vector3(0.0f, 1.0f, 0.0f);
		m_forward = Vector3(0.0f, 0.0f, 1.0f);

		m_pitch = 0.0f;
		m_yaw = 0.0f;
		m_roll = 0.0f;
	}

	// note(tommi): public
	Camera::~Camera()
	{
	}

	void Camera::update()
	{
		if (!m_dirty) return;
		m_dirty = false;

		// todo(tommi): impl
		Vector3 r(1.0f, 0.0f, 0.0f);
		Vector3 u(0.0f, 1.0f, 0.0f);
		Vector3 f(0.0f, 0.0f, 1.0f);

		Matrix4 ry = Matrix4::rotation(u, m_yaw);
		r = ry * r;
		f = ry * f;
		r.normalize();
		f.normalize();

		Matrix4 rx = Matrix4::rotation(r, m_pitch);
		u = rx * u;
		f = rx * f;
		u.normalize();
		f.normalize();

		Matrix4 rz = Matrix4::rotation(f, m_roll);
		r = rz * r;
		u = rz * u;
		r.normalize();
		u.normalize();
		
		// note(tommi): construct view matrix manually
		m_view.identity();
		m_view.m._11 = r.m_x;
		m_view.m._21 = r.m_y;
		m_view.m._31 = r.m_z;

		m_view.m._12 = u.m_x;
		m_view.m._22 = u.m_y;
		m_view.m._32 = u.m_z;

		m_view.m._13 = f.m_x;
		m_view.m._23 = f.m_y;
		m_view.m._33 = f.m_z;

		m_view.m._41 = -m_position.dot(r);
		m_view.m._42 = -m_position.dot(u);
		m_view.m._43 = -m_position.dot(f);

		// note(tommi): save directions to enable movement
		m_right = r;
		m_up = u;
		m_forward = f;
	}

	void Camera::rotate_x(float32 radians) {
		m_dirty = true;
		m_pitch += radians;
	}

	void Camera::rotate_y(float32 radians) {
		m_dirty = true;
		m_yaw += radians;
	}

	void Camera::rotate_z(float32 radians) {
		m_dirty = true;
		m_roll += radians;
	}

	void Camera::move_x(float32 units) {
		m_dirty = true;
		m_position += m_right * units;
	}

	void Camera::move_y(float32 units)
	{
		m_dirty = true;
		m_position += m_up * units;
	}

	void Camera::move_z(float32 units)
	{
		m_dirty = true;
		m_position += m_forward * units;
	}

	const Matrix4& Camera::get_projection() const
	{
		return m_projection;
	}

	const Matrix4& Camera::get_view() const
	{
		return m_view;
	}

	const Vector3& Camera::get_position() const
	{
		return m_position;
	}
}
