
#include <stdio.h>
#include <cassert>
#include <d3d11.h>

#include <zinc/service_locator.hpp>
#include <zinc/render_context.hpp>
#include <zinc/rasterizer_state.hpp>

#define SAFE_RELEASE(x) if(x!=nullptr) { x->Release(); x = nullptr; }

namespace zinc
{
	RasterizerState::Ptr RasterizerState::create(CullMode cull_mode, FillMode fill_mode)
	{
		RenderContext* render_context = ServiceLocator<RenderContext>::get_service();
		ID3D11Device* device = render_context->get_device();

		const D3D11_CULL_MODE cullmodes[]=
		{
			D3D11_CULL_NONE,
			D3D11_CULL_BACK,
			D3D11_CULL_FRONT,
		};

		const D3D11_FILL_MODE fillmodes[]=
		{
			D3D11_FILL_SOLID,
			D3D11_FILL_WIREFRAME,
		};

		D3D11_RASTERIZER_DESC desc;
		desc.FillMode = fillmodes[fill_mode];	//skall vi ha en ifylld triangel - raster �r fylla i pixel f�rger
		desc.CullMode = cullmodes[cull_mode];	//vad ser vi och ska n�got tas bort / det som inte sysns d�
		desc.FrontCounterClockwise = FALSE;		//vilket h�ll vi skall g� i rasterizeringen
		desc.DepthBias = 0;
		desc.SlopeScaledDepthBias = 0.0f;
		desc.DepthBiasClamp = 0.0f;
		desc.DepthClipEnable = TRUE;
		desc.ScissorEnable = FALSE;
		desc.MultisampleEnable = FALSE;
		desc.AntialiasedLineEnable = FALSE;

		ID3D11RasterizerState* rasterizer_state = nullptr;
		HRESULT hr = device->CreateRasterizerState(&desc, &rasterizer_state);
		if (hr != S_OK)
		{
			assert(false && "could not create rasterizer state");
		}

		RasterizerState* result = new RasterizerState;
		result->m_rasterizer_state = rasterizer_state;
		result->m_context = render_context->get_context();
		return Ptr(result);
	}

	RasterizerState::RasterizerState()
	{
		m_rasterizer_state = nullptr;
		m_context = nullptr;
	}

	RasterizerState::~RasterizerState()
	{
		SAFE_RELEASE(m_rasterizer_state);
	}

	void RasterizerState::use()
	{
		m_context->RSSetState(m_rasterizer_state);
	}
}