
#include <stdio.h>
#include <cassert>

#include <zinc\camera.hpp>
#include <zinc\3D_Cube.hpp>

namespace zinc
{
	Cube::Ptr Cube::create(const char* texture_filename)
	{
		struct Vertex		//kuberna
		{
			zinc::Vector3 position;
			zinc::Vector2 texcoord;
			zinc::Vector3 normal;
		};

		//positionerna i boxen
		float32 v = 0.5f;
		Vector3 positions[] =
		{
			Vector3(-v,  v, -v),
			Vector3(v,  v, -v),
			Vector3(v, -v, -v),
			Vector3(-v, -v, -v),

			Vector3(-v, v, v),
			Vector3(v, v, v),
			Vector3(v, -v, v),
			Vector3(-v, -v, v),
		};


		//normalen f�r 3D hantering
		Vector3 normals[] =
		{
			Vector3(0.0f, 0.0f, -1.0f),
			Vector3(1.0f, 0.0f, 0.0f),
			Vector3(0.0f, 0.0f, 1.0f),
			Vector3(-1.0f, 0.0f, 0.0f),
			Vector3(0.0f, 1.0f, 0.0f),
			Vector3(0.0f,-1.0f, 0.0f),
		};

		//texture position f�r minecraft block
		const float32 o = 1.0f / 16.0f;
		Vector2 texcoords[] =
		{
			Vector2(3.0f * o, 0.0f),
			Vector2(4.0f * o, 0.0f),
			Vector2(4.0f * o, o),
			Vector2(3.0f * o, o),

			Vector2(2.0f * o, 0.0f),
			Vector2(3.0f * o, 0.0f),
			Vector2(3.0f * o, o),
			Vector2(2.0f * o, o),

			Vector2(0.0f, 0.0f),
			Vector2(o, 0.0f),
			Vector2(o, o),
			Vector2(0.0f, o),
		};

		//3D blocken
		Vertex vertices[] =
		{
			///Front
			{ positions[0], texcoords[0], normals[0] },
			{ positions[1], texcoords[1], normals[0] },
			{ positions[2], texcoords[2], normals[0] },
			{ positions[2], texcoords[2], normals[0] },
			{ positions[3], texcoords[3], normals[0] },
			{ positions[0], texcoords[0], normals[0] },
			///Right
			{ positions[1], texcoords[0], normals[1] },
			{ positions[5], texcoords[1], normals[1] },
			{ positions[6], texcoords[2], normals[1] },
			{ positions[6], texcoords[2], normals[1] },
			{ positions[2], texcoords[3], normals[1] },
			{ positions[1], texcoords[0], normals[1] },
			///Back
			{ positions[5], texcoords[0], normals[2] },
			{ positions[4], texcoords[1], normals[2] },
			{ positions[7], texcoords[2], normals[2] },
			{ positions[7], texcoords[2], normals[2] },
			{ positions[6], texcoords[3], normals[2] },
			{ positions[5], texcoords[0], normals[2] },
			///Left
			{ positions[4], texcoords[0], normals[3] },
			{ positions[0], texcoords[1], normals[3] },
			{ positions[3], texcoords[2], normals[3] },
			{ positions[3], texcoords[2], normals[3] },
			{ positions[7], texcoords[3], normals[3] },
			{ positions[4], texcoords[0], normals[3] },
			//Top
			{ positions[4], texcoords[8], normals[4] },
			{ positions[5], texcoords[9], normals[4] },
			{ positions[1], texcoords[10], normals[4] },
			{ positions[1], texcoords[10], normals[4] },
			{ positions[0], texcoords[11], normals[4] },
			{ positions[4], texcoords[8], normals[4] },
			//Bottom
			{ positions[3], texcoords[4], normals[5] },
			{ positions[2], texcoords[5], normals[5] },
			{ positions[6], texcoords[6], normals[5] },
			{ positions[6], texcoords[6], normals[5] },
			{ positions[7], texcoords[7], normals[5] },
			{ positions[3], texcoords[4], normals[5] },
		};

		uint32 count = sizeof(vertices) / sizeof(vertices[0]);
		Mesh::Ptr mesh = Mesh::create(
			sizeof(Vertex),
			count,
			vertices,
			0, nullptr);

		Cube* result = new Cube;
		result->m_count = count;
		result->m_texture = Texture::create(texture_filename);
		result->m_mesh = std::move(mesh);
		result->m_sampler = Sampler::create(
			kTextureFilterMode_Linear,
			kTextureAddressMode_Clamp,
			kTextureAddressMode_Clamp);
		result->m_depth_state = DepthState::create(true, true);
		result->m_rasterizer_state = RasterizerState::create(kCullMode_Back, kFillMode_Solid);
		result->m_axisbox = new AxisAlignedBoundingBox(Vector3(0.0f, 0.0f, 0.0f), Vector3(v, v, v));
		return Ptr(result);
	}

	Cube::Cube()
	{
		m_camera = nullptr;
		m_color_shader = nullptr;
		m_count = 0;
		m_position = Vector3(0.0f, 0.0f, 0.0f);
	}

	Cube::~Cube()
	{
	}

	void Cube::draw()
	{
		// note(tommi): example world transforms for two cubes
		Matrix4 world;
		world.identity();
		
		m_rotation += 0.005f;
		zinc::Matrix4 t, r, s;
		t = zinc::Matrix4::translation(m_position);	//dess position
		zinc::Vector3 axis(0.5f, 1.0f, 1.5f);				//dess vridningsriktning
		axis.normalize();
		r = zinc::Matrix4::rotation(axis, m_rotation);
		//s = zinc::Matrix4::scale(m_scale);
		world = r * t;

		m_color_shader->set_view_and_projection(
			m_camera->get_view(),
			m_camera->get_projection());
		m_color_shader->set_world(world);

		m_color_shader->use();
		m_sampler->use();
		m_texture->use();
		m_depth_state->use();
		m_rasterizer_state->use();

		m_mesh->draw(0, m_count);
	}

	void Cube::set_camera(Camera* camera)
	{
		m_camera = camera;
	}

	void Cube::set_color_shader(ColorShader* shader)
	{
		m_color_shader = shader;
	}

	void Cube::set_position(Vector3 position)
	{
		m_position = position;
		m_axisbox->m_center = m_position;
	}

	void Cube::set_scale(Vector3 scale)
	{
		m_scale = scale;
	}
}