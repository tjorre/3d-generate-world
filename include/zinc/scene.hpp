//#pragma once
//#include <memory>
//#include <string>
//#include <vector>
//
//#include <zinc.hpp>
//
//namespace zinc
//{
//	class Mesh;
//	class Texture;
//
//	namespace scene
//	{
//		struct LightProperty
//		{
//			LightProperty()
//			{
//				Ka = 1.0f;
//				Kd = 1.0f;
//				Ks = 0.5f;
//				A = 64.0f;
//			}
//
//			float32 Ka;
//			float32 Kd;
//			float32 Ks;
//			float32 A;
//		};
//
//		class Material
//		{
//			// non-copyable
//			Material(const Material&);
//			Material& operator=(const Material&);
//
//		public:
//			Material();
//			~Material();
//
//			void set_texture(Texture* texture);
//			void set_property(const LightProperty& prop);
//			void set_parameter(const std::string& name, const Vector4& value);
//
//		private:
//			typedef std::pair<std::string, Vector4> Parameter;
//
//			Texture* m_texture;
//			LightProperty m_property;
//			std::vector<Parameter> m_parameters;
//		};
//
//		class Model
//		{
//			// non-copyable
//			Model(const Model&);
//			Model& operator=(const Model&);
//
//		public:
//			typedef std::unique_ptr<Model> Ptr;
//
//			static Ptr create(const char* filename);
//
//		protected:
//			Model();
//
//		public:
//			virtual ~Model();
//
//			uint32 get_mesh_count() const;
//			Mesh* get_mesh(uint32 index);
//
//		protected:
//			std::vector<Mesh*> m_meshes;
//		};
//
//		struct Transform
//		{
//			Transform();
//
//			Vector3 position;
//			Vector3 rotation;
//			Vector3 scale;
//		};
//
//		class SceneNode
//		{
//			friend class Scene;
//			// non-copyable
//			SceneNode(const SceneNode&);
//			SceneNode& operator=(const SceneNode&);
//
//		protected:
//			SceneNode();
//
//			virtual ~SceneNode();
//
//		public:
//			virtual void set_model(Model* mode);
//			virtual void set_material(Material* material);
//
//			void attach_child(SceneNode* node);
//			void detach_child(SceneNode* node);
//			void detach_children();
//
//			void apply_transform_to_children();
//
//			void set_position(const Vector3& position);
//			void set_rotation(const Vector3& rotation);
//			void set_scale(const Vector3& scale);
//			Vector3 get_position() const;
//			Vector3 get_rotation() const;
//			Vector3 get_scale() const;
//
//		protected:
//			std::string m_name;
//			Model* m_model;
//			Material* m_material;
//			Transform m_transform;
//
//			std::vector<SceneNode*> m_children;
//		};
//
//
//		class Scene
//		{
//			// non-copyable
//			// K.I.S.S
//			// Y.A.G.N.I.
//			Scene(const Scene&);
//			Scene& operator=(const Scene&);
//
//		public:
//			typedef std::unique_ptr<Scene> Ptr;
//
//			static Ptr create(const std::string& name);
//
//		protected:
//			Scene(const std::string& name);
//
//		public:
//			virtual ~Scene() {}
//
//			virtual void update();
//			virtual void draw();
//
//			virtual SceneNode* create_scene_node(const std::string& name, bool attach = true);
//			virtual SceneNode* create_scene_node(bool attach = true);
//			virtual void destroy_scene_node(SceneNode* node);
//			virtual void attach_scene_node(SceneNode* node);
//			virtual void detach_scene_node(SceneNode* node);
//			virtual void detach_scene_node_by_name(const std::string& name);
//
//		protected:
//			std::string m_name;
//			std::vector<SceneNode*> m_nodes;
//		};
//
//		class SceneManager
//		{
//			// non-copyable
//			SceneManager(const SceneManager&);
//			SceneManager& operator=(const SceneManager&);
//
//		public:
//			typedef std::unique_ptr<SceneManager> Ptr;
//
//			static Ptr create();
//
//		private:
//			SceneManager();
//
//		public:
//			~SceneManager();
//
//			void attach_scene(Scene* scene);
//			void detach_scene(Scene* scene);
//			void detach_scene_by_name(const std::string& name);
//
//			void update();
//
//			Scene* get_scene_from_name(const std::string& name);
//
//		private:
//			std::vector<Scene*> m_scenes;
//		};
//	}
//}