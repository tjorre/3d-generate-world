#pragma once

#include <zinc.hpp>

struct ID3D11RasterizerState;
struct ID3D11DeviceContext;

namespace zinc
{
	enum CullMode
	{
		kCullMode_None,
		kCullMode_Back,
		kCullMode_Front,
		kCullMode_Invalid,
	};

	enum FillMode
	{
		kFillMode_Solid,
		kFillMode_WireFrame,
		kFillMode_Invalid,
	};

	class RasterizerState
	{
		RasterizerState(const RasterizerState&);
		RasterizerState& operator=(const RasterizerState&);

	public:
		typedef std::unique_ptr<RasterizerState> Ptr;
		static Ptr create(CullMode cull_mode, FillMode fill_mode);

	private:
		RasterizerState();

	public:
		~RasterizerState();

		void use();

	private:
		ID3D11RasterizerState* m_rasterizer_state;
		ID3D11DeviceContext* m_context;
	};
}