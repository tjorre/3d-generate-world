#pragma once

#include <zinc.hpp>
#include <zinc\skybox_shader.hpp>
#include <zinc\texture.hpp>
#include <zinc\mesh.hpp>
#include <zinc\sampler.hpp>
#include <zinc\depth_state.hpp>
#include <zinc\rasterizer_state.hpp>

namespace zinc
{
	class Camera;

	class Skybox
	{
		Skybox(const Skybox&);
		Skybox& operator=(const Skybox&);

	public:
		typedef std::unique_ptr<Skybox> Ptr;

		static Ptr create(const char* texture_filename, const char* shader_filename);

	private:
		Skybox();

	public:
		~Skybox();

		void draw();

		void set_camera(Camera* camera);

	private:
		Camera* m_camera;
		uint32 m_count;
		SkyboxShader::Ptr m_shader;
		Texture::Ptr m_texture;
		Sampler::Ptr m_sampler;
		Mesh::Ptr m_mesh;
		DepthState::Ptr m_depth_state;
		RasterizerState::Ptr m_rasterizer_state;
	};
}