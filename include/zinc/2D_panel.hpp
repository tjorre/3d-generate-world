#pragma once

#include <zinc.hpp>
#include <zinc\othro_shader.hpp>
#include <zinc\texture.hpp>
#include <zinc\mesh.hpp>
#include <zinc\sampler.hpp>
#include <zinc\depth_state.hpp>

namespace zinc
{
	class OthroShader;

	class GUI
	{
		GUI(const GUI&);
		GUI& operator=(const GUI&);

	public:
		typedef std::unique_ptr<GUI> Ptr;
		static Ptr create(const char* texture_filename);

	private:
		GUI();

	public:
		~GUI();

		void draw();

		void set_projection(Matrix4 projection);
		void set_othro_shader(OthroShader* shader);
		void set_position(Vector3 position);
		void set_size(Vector3 size);

		Vector3 get_posistion();
		AxisAlignedBoundingBox* m_axisbox;

	private:
		OthroShader* m_shader;

		uint32 m_count;
		float32 m_rotation;
		Vector3 m_position;
		Vector3 m_size;
		Matrix4 m_projection;

		Texture::Ptr m_texture;
		Sampler::Ptr m_sampler;
		Mesh::Ptr m_mesh;
		DepthState::Ptr m_depth_state;

	};
}
