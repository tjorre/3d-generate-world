#pragma once

#include <zinc.hpp>
#include <zinc\color_shader.hpp>
#include <zinc\texture.hpp>
#include <zinc\mesh.hpp>
#include <zinc\sampler.hpp>
#include <zinc\depth_state.hpp>
#include <zinc\rasterizer_state.hpp>

namespace zinc
{
	class ColorShader;
	class Camera;

	class Cube
	{
		Cube(const Cube&);
		Cube& operator=(const Cube&);

	public:
		typedef std::unique_ptr<Cube> Ptr;
		static Ptr create(const char* texture_filename);

	private:
		Cube();

	public:
		~Cube();

		void draw();

		void set_camera(Camera* camera);
		void set_color_shader(ColorShader* shader);
		void set_position(Vector3 position);
		void set_scale(Vector3 scale);
	
		AxisAlignedBoundingBox* m_axisbox;

	private:
		Camera* m_camera;
		ColorShader* m_color_shader;
		
		uint32 m_count;
		float32 m_rotation;
		Vector3 m_position;
		Vector3 m_scale;

		Texture::Ptr m_texture;
		Sampler::Ptr m_sampler;
		Mesh::Ptr m_mesh;
		DepthState::Ptr m_depth_state;
		RasterizerState::Ptr m_rasterizer_state;
	};
}