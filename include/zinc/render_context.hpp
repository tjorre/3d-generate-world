// render_context.hpp

#ifndef RENDERCONTEXT_HPP_INCLUDED
#define RENDERCONTEXT_HPP_INCLUDED

#include <zinc.hpp>

struct IDXGISwapChain;
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11RenderTargetView;
struct ID3D11Texture2D;
struct ID3D11DepthStencilView;

namespace zinc
{
	class RenderContext
	{
		// non-copyable
		RenderContext(const RenderContext&);
		RenderContext& operator=(const RenderContext&);

	public:
		typedef std::unique_ptr<RenderContext> Ptr;

		static Ptr create(void* window, int32 width, int32 height);

	private:
		RenderContext();

	public:
		~RenderContext();

		void destroy();

		void clear();
		void present();

		ID3D11Device* get_device();
		ID3D11DeviceContext* get_context();

	private:
		IDXGISwapChain* m_swap_chain;
		ID3D11Device* m_device;
		ID3D11DeviceContext* m_context;
		ID3D11RenderTargetView* m_rtv;
		ID3D11Texture2D* m_depth_stencil_buffer;
		ID3D11DepthStencilView* m_dsv;
	};
}

#endif // RENDERCONTEXT_HPP_INCLUDED
