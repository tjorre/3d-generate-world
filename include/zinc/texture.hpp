// texture.hpp

#ifndef TEXTURE_HPP_INCLUDED
#define TEXTURE_HPP_INCLUDED

#include <zinc.hpp>

struct ID3D11Texture2D;
struct ID3D11ShaderResourceView;
struct ID3D11DeviceContext;

namespace zinc
{
	class Texture
	{
		// non-copyable
		Texture(const Texture&);
		Texture& operator=(const Texture&);

	public:
		typedef std::unique_ptr<Texture> Ptr;

		static Ptr create(const char* filename);

	private:
		Texture();

	public:
		~Texture();

		void use();

	private:
		ID3D11Texture2D* m_texture;
		ID3D11ShaderResourceView* m_srv;
		ID3D11DeviceContext* m_context;
	};
}

#endif // TEXTURE_HPP_INCLUDED
