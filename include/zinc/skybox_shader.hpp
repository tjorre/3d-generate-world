#pragma once

#include <zinc.hpp>
#include <zinc/shader.hpp>

struct ID3D11Buffer;

namespace zinc
{
	class SkyboxShader : public Shader
	{
		SkyboxShader(const SkyboxShader&);
		SkyboxShader& operator=(const SkyboxShader&);

	public:
		typedef std::unique_ptr<SkyboxShader> Ptr;

		static Ptr create(const char* filename);

	private:
		SkyboxShader(const char* vertex_source, const char* pixel_source);

	public:
		~SkyboxShader();

		void use();
		void set_view_and_projection(const Matrix4& view, const Matrix4& projection);

	private:
		ID3D11Buffer* m_once_per_frame;
		ID3D11Buffer* m_once_per_object;
	};
}