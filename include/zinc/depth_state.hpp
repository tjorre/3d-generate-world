#pragma once

#include <zinc.hpp>

struct ID3D11DepthStencilState;
struct ID3D11DeviceContext;

namespace zinc
{
	class DepthState
	{
		DepthState(const DepthState&);
		DepthState& operator=(const DepthState&);

	public:
		typedef std::unique_ptr<DepthState> Ptr;

		static Ptr create(bool read, bool write);

	private:
		DepthState();

	public:
		~DepthState();

		void use();

	private:
		ID3D11DepthStencilState* m_depth_state;
		ID3D11DeviceContext* m_context;
	};
}