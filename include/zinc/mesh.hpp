// mesh.hpp

#ifndef MESH_HPP_INCLUDED
#define MESH_HPP_INCLUDED

#include <zinc.hpp>

struct ID3D11Buffer;
struct ID3D11DeviceContext;

namespace zinc
{
	class Mesh
	{
		// non-copyable
		Mesh(const Mesh&);
		Mesh& operator=(const Mesh&);

	public:
		typedef std::unique_ptr<Mesh> Ptr;

		static Ptr create(uint32 size, uint32 count, const void* buffer, uint32 num_indices, const uint16* indices);

	private:
		Mesh(uint32 size, uint32 count, const void* buffer, uint32 num_indices, const uint16* indices);

	public:
		~Mesh();

		void draw(uint32 start, uint32 count);

	private:
		uint32 m_stride;
		ID3D11Buffer* m_vertex_buffer;
		ID3D11Buffer* m_index_buffer;
		ID3D11DeviceContext* m_context;
	};
}

#endif // MESH_HPP_INCLUDED
