// color_shader.hpp

#ifndef COLORSHADER_HPP_INCLUDED
#define COLORSHADER_HPP_INCLUDED

#include <zinc.hpp>
#include <zinc/shader.hpp>

struct ID3D11Buffer;

namespace zinc
{
	struct Material
	{
		float32 Ka;
		float32 Kd;
		float32 Ks;
		float32 shininess;
	};

	struct Light // directional
	{
		Vector4 direction;
		Vector4 ambient;
		Vector4 diffuse;
		Vector4 eye;
	};

	class ColorShader : public Shader
	{
		// non-copyable
		ColorShader(const ColorShader&);
		ColorShader& operator=(const ColorShader&);

	public:
		typedef std::unique_ptr<ColorShader> Ptr;

		static Ptr create(const char* filename);

	private:
		ColorShader(const char* vertex_source, const char* pixel_source);

	public:
		~ColorShader();

		virtual void use();

		void set_view_and_projection(const Matrix4& view, const Matrix4& projection);
		void set_world(const Matrix4& world);

		void set_material(const Material& material);
		void set_light(const Light& light);

	private:
		ID3D11Buffer* m_once_per_frame;
		ID3D11Buffer* m_once_per_object;
		ID3D11Buffer* m_material;
		ID3D11Buffer* m_light;
	};
}

#endif // COLORSHADER_HPP_INCLUDED
