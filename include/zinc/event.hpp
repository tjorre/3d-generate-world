// event.hpp

#ifndef EVENT_HPP_INCLUDED
#define EVENT_HPP_INCLUDED

#include <zinc.hpp>

namespace zinc
{
	enum EventType
	{
		kEventType_MouseMove,
		kEventType_MouseDown,
		kEventType_MouseUp,
		kEventType_MouseWheel,
		kEventType_KeyDown,
		kEventType_KeyUp,
		kEventType_Invalid,
	};

	enum KeyCode
	{
		kKeyCode_Left,
		kKeyCode_Right,
		kKeyCode_Up,
		kKeyCode_Down,
		kKeyCode_Invalid,
	};

	enum MouseButton
	{
		kMouseButton_Left,
		kMouseButton_Middle,
		kMouseButton_Right,
		kMouseButton_Invalid,
	};

	class Event
	{
	public:
		Event();
		Event(const Event& rhs);

		Event& operator=(const Event& rhs);

		EventType get_type() const;
		Vector2 get_mouse() const;
		MouseButton get_button() const;
		KeyCode get_keycode() const;

		EventType m_type;
		Vector2 m_mouse;
		MouseButton m_button;
		KeyCode m_keycode;
	};
}

#endif // EVENT_HPP_INCLUDED
