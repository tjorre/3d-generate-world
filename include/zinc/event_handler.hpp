// event_handler.hpp

#ifndef EVENTHANDLER_HPP_INCLUDED
#define EVENTHANDLER_HPP_INCLUDED

#include <list>
#include <zinc.hpp>
#include <zinc/functor.hpp>

namespace zinc
{
	class Event;

	class EventHandler
	{
		// non-copyable
		EventHandler(const EventHandler&);
		EventHandler& operator=(const EventHandler&);

	public:
		typedef std::unique_ptr<EventHandler> Ptr;

		static Ptr create();

	private:
		EventHandler();

	public:
		~EventHandler();

		template <typename T>
		void register_handler(T* object, void(T::*method)(Event* event))
		{
			Functor<T, Event*>* functor = new Functor<T, Event*>(object, method);
			m_handlers.push_back(functor);
		}

		template <typename T>
		void unregister_handler(T* object)
		{
			HandlerIterator itr = m_handlers.begin();
			while (itr != m_handlers.end())
			{
				if ((*itr) == object) // todo(tommi): we need to fix this
				{
					delete (*itr);
					m_handlers.erase(itr);
					break;
				}
				++itr;
			}
		}

		void notify(Event* event);

	private:
		typedef std::list<IFunctor<Event*>*> HandlerList;
		typedef std::list<IFunctor<Event*>*>::iterator HandlerIterator;

		std::list<IFunctor<Event*>*> m_handlers;
	};
}

#endif // EVENTHANDLER_HPP_INCLUDED
