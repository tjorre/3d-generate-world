// camera.hpp

#ifndef CAMERA_HPP_INCLUDED
#define CAMERA_HPP_INCLUDED

#include <zinc.hpp>

namespace zinc
{
	class Camera
	{
		// non-copyable
		Camera(const Camera&);
		Camera& operator=(const Camera&);

	public:
		typedef std::unique_ptr<Camera> Ptr;

		static Ptr create(const Matrix4& projection);

	private:
		Camera(const Matrix4& projection);

	public:
		~Camera();

		void update();

		void rotate_x(float32 radians);
		void rotate_y(float32 radians);
		void rotate_z(float32 radians);
		void move_x(float32 units);
		void move_y(float32 units);
		void move_z(float32 units);

		const Matrix4& get_projection() const;
		const Matrix4& get_view() const;
		const Vector3& get_position() const;

	private:
		bool m_dirty;
		Matrix4 m_projection;
		Matrix4 m_view;

		Vector3 m_position;
		Vector3 m_right;
		Vector3 m_up;
		Vector3 m_forward;
		
		float32 m_pitch;
		float32 m_yaw;
		float32 m_roll;
	};
}

#endif // CAMERA_HPP_INCLUDED
