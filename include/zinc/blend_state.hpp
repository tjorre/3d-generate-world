#pragma once

#include <zinc.hpp>


struct ID3D11BlendState;
struct ID3D11DeviceContext;

namespace zinc
{
	class BlendState
	{
		BlendState(const BlendState&);
		BlendState& operator=(const BlendState&);

	public:
		typedef std::unique_ptr<BlendState> Ptr;

		static Ptr create();
		//static Ptr create(const char* filename);

	private:
		BlendState();
		//BlendState(const char* vertex_source, const char* pixel_source);

	public:
		~BlendState();

		void use();

	private:
		ID3D11BlendState* m_blend_state;
		ID3D11DeviceContext* m_context;
		float m_blendFactor[4];
	};
}