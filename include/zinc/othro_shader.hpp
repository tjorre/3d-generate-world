#pragma once
#pragma once

#include <zinc.hpp>
#include <zinc\shader.hpp>

struct ID3D11Buffer;

namespace zinc
{
	class OthroShader : public Shader
	{
		// non-copyable
		OthroShader(const OthroShader&);
		OthroShader& operator=(const OthroShader&);

	public:
		typedef std::unique_ptr<OthroShader> Ptr;

		static Ptr create(const char* filename);

	private:
		OthroShader(const char* vertex_source, const char* pixel_source);

	public:
		~OthroShader();

		virtual void use();

		void set_projection(const Matrix4& projection);
		void set_world(const Matrix4& world);

	private:
		ID3D11Buffer* m_once_per_frame;
		ID3D11Buffer* m_once_per_object;
	};
}