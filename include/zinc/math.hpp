// math.hpp

#ifndef MATH_HPP_INCLUDED
#define MATH_HPP_INCLUDED

#include <zinc.hpp>

namespace zinc
{
	class Math
	{
	public:
		static const float32 PI;

		static float32 sqrt(float32 value);
		static float32 sin(float32 radian);
		static float32 cos(float32 radian);
		static float32 tan(float32 radian);
		static float32 to_rad(float32 degrees);
		static float32 to_deg(float32 radians);

		template <typename T>
		static T maxi(T a, T b)
		{
			return a > b ? a : b;
		}

		template <typename T>
		static T mini(T a, T b)
		{
			return a < b ? a : b;
		}

		template <typename T>
		static T abs(T a)
		{
			return a < 0 ? -a : a;
		}

		template <typename T>
		static T clamp(T v, T a, T b)
		{
			return v < a ? a : v > b ? b : v;
		}

		template <typename T>
		static bool are_equal(const T& a, const T& b) { return a == b; }
		template <>
		static bool are_equal<float32>(const float32& a, const float32& b);
		template <>
    static bool are_equal<float64>(const float64& a, const float64& b);
	};

	class Vector2
	{
	public:
		Vector2();
		Vector2(float32 x, float32 y);
		Vector2(const Vector2& rhs);

		Vector2& operator=(const Vector2& rhs);
		Vector2& operator+=(const Vector2& rhs);
		Vector2& operator-=(const Vector2& rhs);
		Vector2& operator*=(const float32 real);
		Vector2& operator/=(const float32 real);
		Vector2 operator+(const Vector2&rhs);
		Vector2 operator-(const Vector2&rhs);
		Vector2 operator*(const float32 real);
		Vector2 operator/(const float32 real);

		float32 length() const;
		float32 length_squared() const;
		float32 dot(const Vector2& rhs) const;
		void normalize();

		float32 m_x;
		float32 m_y;
	};

	class Vector3
	{
	public:
		Vector3();
		Vector3(float32 x, float32 y, float32 z);
		Vector3(const Vector3& rhs);

		Vector3& operator=(const Vector3& rhs);
		Vector3& operator+=(const Vector3& rhs);
		Vector3& operator-=(const Vector3& rhs);
		Vector3& operator*=(const float32 real);
		Vector3& operator/=(const float32 real);
		Vector3 operator+(const Vector3& rhs);
		const Vector3 operator+(const Vector3& rhs) const;
		Vector3 operator-(const Vector3& rhs);
		Vector3 operator*(const float32 real);
		Vector3 operator/(const float32 real);

		Vector3 cross(const Vector3& rhs);

		float32 length() const;
		float32 length_squared() const;
		float32 dot(const Vector3& rhs) const;
		void normalize();

		float32 m_x;
		float32 m_y;
		float32 m_z;
	};

	class Vector4
	{
	public:
		Vector4();
		Vector4(float32 x, float32 y, float32 z, float32 w);
		Vector4(const Vector4& rhs);
		Vector4(const Vector3& rhs, float32 d);
		Vector4& operator=(const Vector4& rhs);

		float32 m_x;
		float32 m_y;
		float32 m_z;
		float32 m_w;
	};

	class Matrix4
	{
	public:
		Matrix4();
		Matrix4(float32 _11, float32 _12, float32 _13, float32 _14,
			float32 _21, float32 _22, float32 _23, float32 _24,
			float32 _31, float32 _32, float32 _33, float32 _34,
			float32 _41, float32 _42, float32 _43, float32 _44);
		Matrix4(const Matrix4& rhs);

		Matrix4& operator=(const Matrix4& rhs);
		Matrix4 operator*(const Matrix4 &rhs);
		const Matrix4 operator*(const Matrix4 &rhs) const;

		Vector3 operator*(const Vector3& rhs);

		void identity();
		Matrix4 inverse();

		static Matrix4 scale(float32 x, float32 y, float32 z);
		static Matrix4 scale(const Vector3 &v);
		static Matrix4 rotation(const Vector3 &axis, float32 radians);
		static Matrix4 rotation(float32 x, float32 y, float32 z, float32 radians);
		static Matrix4 translation(float32 x, float32 y, float32 z);
		static Matrix4 translation(const Vector3 &v);
		static Matrix4 perspective(float32 fov, float32 aspect, float32 znear, float32 zfar);
		static Matrix4 orthogonal(float32 width, float32 height, float32 znear, float32 zfar);

		union
		{
			float32 _m[16];
			struct
			{
				float32 _11, _12, _13, _14;
				float32 _21, _22, _23, _24;
				float32 _31, _32, _33, _34;
				float32 _41, _42, _43, _44;
			} m;
		};
	};

	class Ray
	{
	public:
		Ray();
		Ray(const Ray& rhs);
		Ray(const Vector3& origin, const Vector3& direction);

		Ray& operator=(const Ray& rhs);

		Vector3 m_origin;
		Vector3 m_direction;
	};

	class Plane
	{
	public:
		Plane();
		Plane(const Plane& rhs);
		Plane(const Vector3& normal, const float32 D);
		Plane& operator=(const Plane& rhs);

		Vector3 m_normal;
		float32 m_D;
	};

	class BoundingSphere
	{
	public:
		BoundingSphere();
		BoundingSphere(const Vector3& center, float32 radius);
		BoundingSphere(const BoundingSphere& rhs);
		BoundingSphere& operator=(const BoundingSphere& rhs);

		Vector3 m_center;
		float32 m_radius;
	};

	class AxisAlignedBoundingBox
	{
	public:
		AxisAlignedBoundingBox();
		AxisAlignedBoundingBox(const Vector3& center, const Vector3& halfextent);
		AxisAlignedBoundingBox(const AxisAlignedBoundingBox& rhs);
		AxisAlignedBoundingBox& operator=(const AxisAlignedBoundingBox& rhs);
		bool box_contains(Vector3 mpos);
		
		Vector3 m_center, m_extent;
	};

	class Frustum
	{
	public:
		Frustum();
		Frustum(const Frustum& rhs);
		Frustum& operator=(const Frustum& rhs);

		void construct(Matrix4& viewproj);
		const bool is_inside(const Vector3& point) const;
		const bool is_inside(const BoundingSphere& sphere) const;
		const bool is_inside(const AxisAlignedBoundingBox& aabb) const;

		Plane m_planes[6];
	};
}

#endif // MATH_HPP_INCLUDED
