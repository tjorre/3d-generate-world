#pragma once

#include <zinc.hpp>
#include <zinc\texture.hpp>
#include <zinc\mesh.hpp>
#include <zinc\sampler.hpp>
#include <zinc\depth_state.hpp>
#include <zinc\rasterizer_state.hpp>

namespace zinc
{
	class ColorShader;
	class Camera;

	class Terrain
	{
		Terrain(const Terrain&);
		Terrain& operator=(const Terrain&);

	public:
		typedef std::unique_ptr<Terrain> Ptr;

		static Ptr create(const char* filename, const char* texture_filename);
		//static Ptr create(const char* filename);

	private:
		Terrain();

	public:
		~Terrain();

		void draw();

		void set_color_shader(ColorShader* shader);
		void set_camera(Camera* camera);

		//void set_texture(Texture& texture);
		//void set_sampler(Sampler* sampler);

		void set_wireframe(bool state);
		int32 get_vertices();

	private:
		ColorShader* m_shader;
		Camera* m_camera;
			
		Texture::Ptr m_texture;
		Sampler::Ptr m_sampler;
		DepthState::Ptr m_depth_state;
	
		uint32 m_count;
		int32 m_vertices_count;
		Mesh::Ptr m_mesh;

		enum WireframeState
		{
			kWireframeState_On,
			kWireframeState_Off,
			kWireframeState_Invalid,
		};

		struct Vertex
		{
			Vector3 position;
			Vector2 texcoord;
			Vector3 normal;
		};

		WireframeState m_state;
		RasterizerState::Ptr m_rasterizer_state[kWireframeState_Invalid];
	};
}