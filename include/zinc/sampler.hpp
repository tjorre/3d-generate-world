// sampler.hpp

#ifndef SAMPELR_HPP_INCLUDED
#define SAMPELR_HPP_INCLUDED

#include <zinc.hpp>

struct ID3D11SamplerState;
struct ID3D11DeviceContext;

namespace zinc
{
	enum TextureFilterMode
	{
		kTextureFilterMode_Point,
		kTextureFilterMode_Linear,
		kTextureFilterMode_Invalid,
	};

	enum TextureAddressMode
	{
		kTextureAddressMode_Clamp,
		kTextureAddressMode_Wrap,
		kTextureAddressMode_Mirror,
		kTextureAddressMode_Invalid,
	};

	class Sampler
	{
		// non-copyable
		Sampler(const Sampler&);
		Sampler& operator=(const Sampler&);

	public:
		typedef std::unique_ptr<Sampler> Ptr;

		static Ptr create(TextureFilterMode filter, TextureAddressMode addr_u, TextureAddressMode addr_v);

	private:
		Sampler();

	public:
		~Sampler();

		void use();

	private:
		ID3D11SamplerState* m_sampler_state;
		ID3D11DeviceContext* m_context;
	};
}

#endif // SAMPELR_HPP_INCLUDED
