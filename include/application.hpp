// application.hpp

#ifndef APPLICATION_HPP_INCLUDED
#define APPLICATION_HPP_INCLUDED

#include <zinc.hpp>
#include <zinc/render_context.hpp>
#include <zinc/event_handler.hpp>
#include <zinc/camera.hpp>

#include <zinc/othro_shader.hpp>
#include <zinc/color_shader.hpp>
#include <zinc/mesh.hpp>
#include <zinc/math.hpp>

#include <zinc/texture.hpp>
#include <zinc/sampler.hpp>
#include <zinc/depth_state.hpp>
#include <zinc/blend_state.hpp>

#include <zinc/terrain.hpp>
#include <zinc/skybox.hpp>
#include <zinc/3D_cube.hpp>
#include <zinc/2D_panel.hpp>

namespace zinc
{
	class Event;
}

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}

class Application
{
	// note(tommi): non-copyable
	Application(const Application&);
	Application& operator=(const Application&);

public:
	Application();
	~Application();

	bool create(void* window, int32 width, int32 height);
	void destroy();
	void update();

private:
	void on_event(zinc::Event* event);

	void on_click();

private:
	void* m_window;
		//numberic variables
	int32 m_width;
	int32 m_height;
	
	zinc::Matrix4 m_projection;
	zinc::Matrix4 m_othro_projection;
	zinc::Matrix4 m_world;

		//handlers
	zinc::RenderContext::Ptr m_render_context;
	zinc::EventHandler::Ptr m_event_handler;
	zinc::Camera::Ptr m_camera;

	zinc::OthroShader::Ptr m_othro_shader;
	zinc::ColorShader::Ptr m_shader;

	zinc::Mesh::Ptr m_persp_mesh;
	zinc::Mesh::Ptr m_othro_mesh;

	zinc::Texture::Ptr m_texture;
	zinc::Sampler::Ptr m_sampler;
	
	zinc::DepthState::Ptr m_depth_state;
	zinc::BlendState::Ptr m_blend_state;
	zinc::DepthState::Ptr m_othro_depth_state;

	zinc::Material m_material;
	zinc::Light m_light;

		//game objects
	zinc::Terrain::Ptr m_terrain;
	zinc::Skybox::Ptr m_skybox;
	zinc::Cube::Ptr m_cube;

	zinc::GUI::Ptr m_panel;
	zinc::GUI::Ptr m_hmap4;
	zinc::GUI::Ptr m_hmap2;

	zinc::Frustum* m_frustrum;
	zinc::Matrix4 m_viewproject;

	zinc::AxisAlignedBoundingBox* m_cubebox;
	
	DirectX::SpriteBatch* m_sprite_batch;
	DirectX::SpriteFont* m_sprite_font;

		//keyboard and mouse struct
	struct
	{
		bool keys[4];
	} m_keyboard;
	struct
	{
		zinc::Vector2 position;
		bool buttons[zinc::kMouseButton_Invalid];
	} m_mouse;
	zinc::Vector2 m_previous;
};

#endif // APPLICATION_HPP_INCLUDED
