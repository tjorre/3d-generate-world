// zinc.hpp

#ifndef ZINC_HPP_INCLUDED
#define ZINC_HPP_INCLUDED

#include <memory>

typedef unsigned __int64 uint64;
typedef   signed __int64 int64;

typedef unsigned int uint32;
typedef   signed int int32;

typedef unsigned short uint16;
typedef   signed short int16;

typedef unsigned char uint8;
typedef   signed char int8;

typedef double float64;
typedef float float32;

#include <zinc/math.hpp>

#endif // ZINC_HPP_INCLUDED
