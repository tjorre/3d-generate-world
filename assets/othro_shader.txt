[vertex]

struct VertexInput
{
	float3 position : POSITION;	
	float2 texcoord : TEXCOORD;
};

struct PixelInput
{
	//float4 world : POSITION;
	float4 position : SV_POSITION;
	float2 texcoord : TEXCOORD;
}; 

cbuffer OncePerFrame 
{
	matrix projection;
};

cbuffer OncePerObject 
{
	matrix world;
};

PixelInput main(VertexInput input)
{
	float4 position = float4(input.position, 1);
	position = mul(world, position);
	position = mul(projection, position);

	PixelInput output;
	//output.world = mul(world, float4(input.position, 1));
	output.position = position;
	output.texcoord = input.texcoord;
	return output;
}

[pixel]

struct PixelInput
{
	//float4 world : POSITION;
	float4 position : SV_POSITION;
	float2 texcoord : TEXCOORD;
};

Texture2D diffuseTexture;
SamplerState defaultSampler;

float4 main(PixelInput input) : SV_TARGET
{
	return diffuseTexture.Sample(defaultSampler, input.texcoord);
}
